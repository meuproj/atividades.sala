#!/bin/bash

while true; do
    # Exibir o menu
    echo "Menu:"
    echo "a -> Executar ping para um host (ip ou site)"
    echo "b -> Listar os usuários atualmente logados na máquina"
    echo "c -> Exibir o uso de memória e de disco da máquina"
    echo "d -> Sair"

    # Solicitar a escolha do usuário
    read -p "Digite a opção desejada: " opcao

    # Verificar a opção escolhida
    case $opcao in
        a)
            read -p "Digite o host (ip ou site) para executar o ping: " host
            ping -c 4 $host
            ;;
        b)
            who
            ;;
        c)
            echo "Uso de memória:"
            free -m
            echo
            echo "Uso de disco:"
            df -h
            ;;
        d)
            echo "Saindo do script."
            exit 0
            ;;
        *)
            echo "Opção inválida. Tente novamente."
            ;;
    esac
done

